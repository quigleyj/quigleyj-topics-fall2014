// 

#include "stdafx.h"

void error_callback(int error, const char* desc)	{
	//	TODO: windows-specific
	fputws((wchar_t*)desc, stderr);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)	{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

void fb_callback(GLFWwindow* window, int width, int height)	{
	float ratio = width / (float)height;

	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-ratio, ratio, -1.0f, 1.0f, 1.0f, -1.0f);
}

int main()	{
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())	{
		exit(EXIT_FAILURE);
	}
	
	glEnable(GL_TEXTURE_2D);

	GLFWwindow* window = glfwCreateWindow(640, 480, "woah I have a window", NULL, NULL);

	//this feels like SDL
	if (!window)	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	ilInit();
	//ilClearColor(255, 255, 255, 000);

	if (ilGetError() != IL_NO_ERROR)	{
		//	failure initializing DevIL
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, fb_callback);

	double time = 0;

	//	------------------------------------
	// image loading bit
	ILuint imgID = 0;
	ilGenImages(1, &imgID);
	ilBindImage(imgID);

	if (!ilLoadImage((const ILstring)"./data/test.bmp"))	{
		//	failure loading image
		wprintf(L"%x, %s", ilGetError(), L"/data/test.bmp");
		Sleep(5000);
		exit(EXIT_FAILURE);
	}

	if (!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE))	{
		exit(EXIT_FAILURE);
	}

	GLuint texID;
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(
		GL_TEXTURE_2D, 
		0, 
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH), 
		ilGetInteger(IL_IMAGE_HEIGHT),
		0, 
		GL_RGBA, 
		GL_UNSIGNED_BYTE, 
		ilGetData()
	);
	//	end image loading bit
	//	------------------------------------

	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);

	while (!glfwWindowShouldClose(window))	{
		glClear(GL_COLOR_BUFFER_BIT);

		time = glfwGetTime();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glRotatef((GLfloat)(time * 50.0f), 0.0f, 0.0f, 1.0f);

		glBegin(GL_QUADS);
			glTexCoord2f(	0.0f, 0.0f);
			glVertex3f(		-0.6f, -0.6f, 0.0f);
			glTexCoord2f(	0.0f,  1.0f);
			glVertex3f(		-0.6f,  0.6f, 0.0f);
			glTexCoord2f(	1.0f,   1.0f);
			glVertex3f(		0.6f,   0.6f, 0.0f);
			glTexCoord2f(	1.0f,  0.0f);
			glVertex3f(		0.6f,  -0.6f, 0.0f);
		glEnd();

		glfwPollEvents();

		glfwSwapBuffers(window);
	}

	ilDeleteImages(1, &imgID);
	glDeleteTextures(1, &texID);

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}